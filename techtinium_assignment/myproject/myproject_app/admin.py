from django.contrib import admin
from django.contrib import admin

from .models import CountryTableStatic,CapacityCostMachine
# Register your models here.



class CountryTableStaticAdmin(admin.ModelAdmin):
	list_display = ('id','country_name','country_keyword')

	search_fields = ('id','country_name','country_keyword')
	


class CapacityCostMachineAdmin(admin.ModelAdmin):
	list_display = ('id','county_id','machine_name','machine_capacity','machine_cost')

	search_fields = ('id','county_id','machine_name','machine_capacity','machine_cost')
	
	row_id_fields = ('county_id',)
	
	def country(self,obj):
		return obj.county_id.id
	
	
admin.site.register(CapacityCostMachine,CapacityCostMachineAdmin)
admin.site.register(CountryTableStatic,CountryTableStaticAdmin)
