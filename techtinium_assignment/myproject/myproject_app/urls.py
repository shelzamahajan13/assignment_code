from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.static import static
from .import views

urlpatterns = [
	url(r'^$', views.IndexPageOnload, name=''),
	url(r'^submit_query_data/$', views.SubmitDataClick, name='curative-routine'),
]
# if settings.DEBUG:
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
