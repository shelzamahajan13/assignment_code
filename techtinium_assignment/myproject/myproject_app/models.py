from django.db import models
from django.utils import timezone
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class CountryTableStatic(models.Model):
	country_name = models.CharField(max_length=50,null=True,blank=True)
	country_keyword= models.CharField(max_length=50,null=True,blank=True)

class CapacityCostMachine(models.Model):
	county_id=models.ForeignKey(CountryTableStatic,null=True,blank=True,on_delete=models.CASCADE)
	machine_name=models.CharField(max_length=50,null=True,blank=True)
	machine_capacity=models.IntegerField(null=True,blank=True)
	machine_cost=models.CharField(max_length=50,null=True,blank=True)

