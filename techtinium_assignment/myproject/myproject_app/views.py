
from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.views.generic import TemplateView,ListView
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

import logging
import requests
import json
import string,re

import io
import sys
from django.utils import timezone
from datetime import datetime,timedelta
import datetime

# Create your views here.
from .models import CountryTableStatic,CapacityCostMachine

stdlogger = logging.getLogger(__name__)

log = logging.getLogger(__name__)

if __name__ == '__main__':
	sys.exit(main())

# Create your views here.
def IndexPageOnload(request):
	return render(request,'index.html')

@csrf_exempt
def SubmitDataClick(request):
	result_set={}
	try:
		req_data=request.POST.get('data')
		
		req_data=json.loads(req_data)

	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)
	try:
		if request.method=="POST":
			get_region_list=CountryTableStatic.objects.all()

			full_chart_machine_data=[]


			for i in get_region_list:
				region_name=i.country_name
				region_keyword=i.country_keyword
				region_id=i.id

				if region_keyword=='ny':
					if CapacityCostMachine.objects.filter(county_id=region_id).exists():
						get_capacity_value_set=CapacityCostMachine.objects.filter(county_id=region_id)
						
						capacity_array_set_ny={}
						cost_array_set_ny={}
						for  value in get_capacity_value_set:
							machine_name=value.machine_name
							machine_capacity=value.machine_capacity
							result={machine_name:machine_capacity}
							capacity_array_set_ny.update(result)

							log.info(capacity_array_set_ny)

						capacity_val1=req_data['capacity']
						machines_capacity_set={}
						
						for i in capacity_array_set_ny:
							log.info(i)
							if int(capacity_array_set_ny[i]) <= int(capacity_val1):
								
								get_machines_values = int(capacity_val1)//int(capacity_array_set_ny[i])
								
								capacity_val1= int(capacity_val1)-int(capacity_array_set_ny[i])
								log.info(capacity_val1)
								
								
								machines_capacity_set.update({i:get_machines_values})
								
							else:
								machines_capacity_set=machines_capacity_set

						get_cost_value_set=CapacityCostMachine.objects.filter(county_id=region_id)
						log.info(get_cost_value_set)

						for  value in get_cost_value_set:
							machine_name=value.machine_name
							machine_cost=int(value.machine_cost)
							result1={machine_name:machine_cost}
							cost_array_set_ny.update(result1)

							
								

						total_minimal_cost = 0
						
						for cost in cost_array_set_ny:
							
							  
							if cost in machines_capacity_set.keys():
								
								minimal_cost = cost_array_set_ny[cost] * machines_capacity_set[cost]
								
								total_minimal_cost = total_minimal_cost+minimal_cost

						total_minimal_cost=int(total_minimal_cost)*int(req_data['hour'])
						log.info(total_minimal_cost)

						result_set_new={"Total_cost":total_minimal_cost,"Region":region_name,"Machines":machines_capacity_set}
						log.info(result_set_new)

						full_chart_machine_data.append(result_set_new)
								

				elif region_keyword=='in':
					if CapacityCostMachine.objects.filter(county_id=region_id).exists():
						get_capacity_value_set=CapacityCostMachine.objects.filter(county_id=region_id)
						
						capacity_array_set_in={}
						cost_array_set_in={}
						for  value in get_capacity_value_set:
							machine_name=value.machine_name
							machine_capacity=value.machine_capacity
							result={machine_name:machine_capacity}
							capacity_array_set_in.update(result)

							log.info(capacity_array_set_in)

						capacity_val2=req_data['capacity']
						machines_capacity_set={}
						
						for i in capacity_array_set_in:
							log.info(i)
							if int(capacity_array_set_in[i]) <= int(capacity_val2):
								
								get_machines_values = int(capacity_val2)//int(capacity_array_set_in[i])
								
								capacity_val2= int(capacity_val2)-int(capacity_array_set_in[i])
								log.info(capacity_val2)
								
								
								machines_capacity_set.update({i:get_machines_values})
								
							else:
								machines_capacity_set=machines_capacity_set

						get_cost_value_set=CapacityCostMachine.objects.filter(county_id=region_id)
						log.info(get_cost_value_set)

						for  value in get_cost_value_set:
							machine_name=value.machine_name
							machine_cost=int(value.machine_cost)
							result1={machine_name:machine_cost}
							cost_array_set_in.update(result1)

							
								

						total_minimal_cost = 0
						
						for cost in cost_array_set_in:
							
							  
							if cost in machines_capacity_set.keys():
								
								minimal_cost = int(cost_array_set_in[cost]) * int(machines_capacity_set[cost])
								
								total_minimal_cost = total_minimal_cost+minimal_cost

						total_minimal_cost=int(total_minimal_cost)*int(req_data['hour'])
						log.info(total_minimal_cost)

						result_set_new={"Total_cost":total_minimal_cost,"Region":region_name,"Machines":machines_capacity_set}
						log.info(result_set_new)

						full_chart_machine_data.append(result_set_new)

				elif region_keyword=='ch':
					if CapacityCostMachine.objects.filter(county_id=region_id).exists():
						get_capacity_value_set=CapacityCostMachine.objects.filter(county_id=region_id)
						
						capacity_array_set_ch={}
						cost_array_set_ch={}
						for  value in get_capacity_value_set:
							machine_name=value.machine_name
							machine_capacity=value.machine_capacity
							result={machine_name:machine_capacity}
							capacity_array_set_ch.update(result)

							log.info(capacity_array_set_ch)

						capacity_val3=req_data['capacity']
						machines_capacity_set={}
						
						for i in capacity_array_set_ch:
							
							if int(capacity_array_set_ch[i]) <= int(capacity_val3):
								
								get_machines_values = int(capacity_val3)//int(capacity_array_set_ch[i])
								
								capacity_val3= int(capacity_val3)-int(capacity_array_set_ch[i])
								log.info(capacity_val3)
								
								
								machines_capacity_set.update({i:get_machines_values})
								
							else:
								machines_capacity_set=machines_capacity_set

						get_cost_value_set=CapacityCostMachine.objects.filter(county_id=region_id)
						log.info(get_cost_value_set)

						for  value in get_cost_value_set:
							machine_name=value.machine_name
							machine_cost=int(value.machine_cost)
							result1={machine_name:machine_cost}
							cost_array_set_ch.update(result1)

							
								

						total_minimal_cost = 0
						
						for cost in cost_array_set_ch:
							
							  
							if cost in machines_capacity_set.keys():
								
								minimal_cost = int(cost_array_set_ch[cost]) * int(machines_capacity_set[cost])
								
								total_minimal_cost = total_minimal_cost+minimal_cost

						total_minimal_cost=int(total_minimal_cost)*int(req_data['hour'])
						log.info(total_minimal_cost)

						result_set_new={"Total_cost":total_minimal_cost,"Region":region_name,"Machines":machines_capacity_set}
						log.info(result_set_new)

						full_chart_machine_data.append(result_set_new)
				

				log.info(full_chart_machine_data)

				result_set['full_chart_machine_data']=full_chart_machine_data
				


			
			
		return HttpResponse(json.dumps(result_set), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)
		
	
		
