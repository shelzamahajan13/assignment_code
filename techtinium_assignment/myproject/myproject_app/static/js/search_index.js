
app.controller("search_controller",function($scope,$http){

	

	$scope.validation=function(type,value){
		if(type == "capacity"  && value!== undefined){
			
			if(value!=''){
				if(type == "capacity"){
					$scope.capacity_err= ""; 
					return true;
				}
			}else{
				
				$scope.capacity_err = "Enter capacity";
				return false;
			} 
		}else if((type == "capacity") && value == undefined){
				$scope.capacity_err = "Capacity required";
				return false;
		}

		if(type == "hour"  && value!== undefined){
			
			if(value!=''){
				if(type == "hour"){
					$scope.hour_err= ""; 
					return true;
				}
			}else{
				
				$scope.hour_err = "Enter hour";
				return false;
			} 
		}else if((type == "hour") && value == undefined){
				$scope.hour_err = "Hour required";
				return false;
		
		}

		
	}

	$(".table_data").hide()
	$scope.submit_query_data=function(user){
		var capacity=$scope.validation('capacity',$scope.user.capacity)
		var hour=$scope.validation('hour',$scope.user.hour)
		

		var data=angular.copy(user)
		var data=JSON.stringify(data)
		if(capacity==true && hour==true){
			$("#get_button").addClass('disabled')
			$.ajax({
				url:'/submit_query_data/',
				type:'POST',
				dataType: 'json',
				data:{'data':data},
				success: function(response){
					
					var a=response['full_chart_machine_data'];
					console.log(a)

					$(".table_data").show()

					sr_no=0
					$("#fetch_machine_Data").empty()
					for (i=0;i<a.length;i++){
						sr_no=sr_no+1
						var region_name=response['full_chart_machine_data'][i]['Region'];
						var cost=response['full_chart_machine_data'][i]['Total_cost'];
						var Machines_data=JSON.stringify(response['full_chart_machine_data'][i]['Machines'])
						console.log(Machines_data)

						$("#fetch_machine_Data").append("<tr><td>"+sr_no+"</td><td>"+region_name+"</td><td>"+cost+"</td><td>"+Machines_data+"</td></tr>")
					}
					
					$("#get_button").removeClass('disabled')

					
					
				},function (response) {
					if(response.status=="500"){
						console.log('wrong1'); 
					}
				}
			})
		}else{
			alert("Please fill out all the fields")
		}


	}
})