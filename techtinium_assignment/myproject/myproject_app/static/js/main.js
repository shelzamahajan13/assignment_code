var app = angular.module('myApp', []);


app.config(function ($interpolateProvider) {


    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

});



domainName = document.location.origin;
console.log(domainName)

app.constant('CONFIG', {
    'APP_NAME' : 'My Awesome App',
    'APP_VERSION' : '0.0.0',
    'GOOGLE_ANALYTICS_ID' : '',
    'BASE_URL' : domainName+'/',
    'SYSTEM_LANGUAGE' : '',
    'FILE_PATH':domainName+'/static/img/uploads/'
})



